import * as React from "react";
import {
    Grid,
    GridColumn as Column,
    GridToolbar,
    GridItemChangeEvent,
    GridRowClickEvent,
} from "@progress/kendo-react-grid";
import {UserItem} from "../interfaces/index";
import {useHistory} from "react-router-dom";
import {UserStoreImpl} from "../store/user";
import {observer} from "mobx-react-lite";
import {useEffect} from "react";
import SearchUser from "./SearchUser";

interface Props{
    toggleDialog: () => void,
    userStore: UserStoreImpl,
}

const List: React.FC<Props> = observer(({toggleDialog, userStore}) => {
    const history = useHistory()
    const [data, setData] = React.useState<Array<UserItem>>(userStore.users);
    const [editID, setEditID] = React.useState<number | null>(null);
    console.log(typeof new Date() , 'last')

    const rowClick = (event: GridRowClickEvent) => {
        history.push(`/user/${event.dataItem.id}`)
        userStore.getDetailUser(event.dataItem)
    };

    const itemChange = (event: GridItemChangeEvent) => {
        const inEditID = event.dataItem.ProductID;
        const field = event.field || "";
        const newData = data.map((item) =>
            item.id === inEditID ? { ...item, [field]: event.value } : item
        );
        setData(newData);
    };

    const closeEdit = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        if (event.target === event.currentTarget) {
            setEditID(null);
        }
    };

    useEffect(() => {
        userStore.fetchUsers()
    },[userStore])

    return (
        <Grid
            data={data.map((item: UserItem) => ({
                ...item,
                inEdit: item.id === editID,
            }))}

            editField="inEdit"
            onRowClick={rowClick}
            onItemChange={itemChange}
        >
            <GridToolbar>
                <div onClick={event => closeEdit(event)}>
                    <button
                        title="Add new"
                        className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-primary"
                        onClick={toggleDialog}
                    >
                        Add new
                    </button>
                </div>
                <SearchUser userStore={userStore}/>
            </GridToolbar>
            <Column field="id" title="Id" width="50px" editable={false} />
            <Column field="username" title="Username" />
            <Column field="fullName" title="FullName" />
            <Column
                field="lastLogin"
                title="LastLogin"
                editor="date"
                format="{0:d}"
            />
            <Column field="completed" title="Enabled" editor="boolean" />
        </Grid>
    );
});
export default List;