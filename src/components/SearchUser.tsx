import React, {useState} from 'react';
import { Input } from "@progress/kendo-react-inputs";
import {UserStoreImpl} from "../store/user";

interface Props{
    userStore: UserStoreImpl,
}

const SearchUser:React.FC<Props> = ({userStore}) => {
    const [search, setSearch] = useState<any>('')
    React.useEffect(() => {
        userStore.searchUsers(search)
    },[search])
    return (
        <div>
            <Input placeholder={'Search ..'} onChange={e => setSearch(e.target.value)}/>
        </div>
    );
};

export default SearchUser;