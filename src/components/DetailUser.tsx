import React from 'react';
import {UserItem} from "../interfaces";
import {Grid, GridItemChangeEvent, GridRowClickEvent} from "@progress/kendo-react-grid";
import {GridColumn as Column} from "@progress/kendo-react-grid/dist/npm/GridColumn";
import {UserStoreImpl} from "../store/user";
import {observer} from "mobx-react-lite";

interface Props{
    userStore: UserStoreImpl,
}

const DetailUser:React.FC<Props> = observer(({userStore}) => {

    const [data, setData] = React.useState<Array<UserItem>>(userStore.user);
    const [editID, setEditID] = React.useState<number | null>(null);

    const rowClick = (event: GridRowClickEvent) => {
        setEditID(event.dataItem.id);
    };

    const itemChange = (event: GridItemChangeEvent) => {
        const inEditID = event.dataItem.id;
        const field = event.field || "";
        const newData = data.map((item) => {
            if (item.id === inEditID) {
                return {...item, [field]: event.value}
            } else {
                return item
            }
        });

        setData(newData);
    };

    React.useEffect(() => {
        userStore.updateUser(data[0])
    },[data])

    return (
        <Grid
            data={data.map((item: UserItem) => ({...item, inEdit: item.id === editID}))}
            editField="inEdit"
            onRowClick={rowClick}
            onItemChange={itemChange}
        >
            <Column field="id" title="Id" width="50px" editable={false} />
            <Column field="username" title="Username" />
            <Column field="fullName" title="FullName" />
            <Column
                field="lastLogin"
                title="LastLogin"
                editor="date"
                format="{0:d}"
            />
            <Column field="completed" title="Enabled" editor="boolean" />
        </Grid>
    );
});

export default DetailUser;