import React from 'react';
import { Dialog } from "@progress/kendo-react-dialogs";
import {
    Form,
    Field,
    FormElement,
    FieldRenderProps,
    FormRenderProps,
} from "@progress/kendo-react-form";

import { Error } from "@progress/kendo-react-labels";
import { Input } from "@progress/kendo-react-inputs";
import { getter } from "@progress/kendo-react-common";
import {UserStoreImpl} from "../store/user";

interface Props{
    toggleDialog: () => void,
    visible: boolean,
    userStore: UserStoreImpl,
}


const AddUser:React.FC<Props> = ({visible,toggleDialog,userStore}) => {
    const [requestObj , setRequestObj] = React.useState<object>({})

    const usernameGetter: any = getter("username");
    const firstNameGetter: any = getter("firstName");
    const lastNameGetter: any = getter("lastName");


    const usernameRegexp: RegExp = /^[a-z0-9_-]{1,16}$/;
    const usernameValidator = (value: string) => usernameRegexp.test(value) ? "" : "Please enter a valid username.";

    const nameRegexp: RegExp = /^[a-z0-9_-]{1,25}$/;
    const nameValidator = (value: string) => nameRegexp.test(value) ? "" : "Please enter a correct value.";

    const firstOrLastNameValidator = (values: any) => {
        if (firstNameGetter(values) && lastNameGetter(values) && usernameGetter(values)) {
            setRequestObj(values);
            return;
        }
        return {
            VALIDATION_SUMMARY: "Please fill at least one of the following fields.",
            ["username"]:
                "Enter username",
            ["firstName"]:
                "Enter first name",
            ["lastName"]:
                "Enter last name",
        };
    };

    const ValidatedInput = (fieldRenderProps: FieldRenderProps) => {
        const { validationMessage, visited, ...others } = fieldRenderProps;
        return (
            <div>
                <Input {...others} />
                {visited && validationMessage && <Error>{validationMessage}</Error>}
            </div>
        );
    };

    const handleSubmit = () => {
        userStore.addUser(requestObj);
        toggleDialog()
    }

    return (
        <>
            {visible && (
                <Dialog title={"Add new"} onClose={toggleDialog}>
                    <Form
                        onSubmit={handleSubmit}
                        validator={firstOrLastNameValidator}
                        render={(formRenderProps: FormRenderProps) => (
                            <FormElement style={{ width: 400 }}>
                                <fieldset className={"k-form-fieldset"}>
                                    <legend className={"k-form-legend"}>
                                        Please fill in all the fields:
                                    </legend>
                                    {formRenderProps.visited &&
                                    formRenderProps.errors &&
                                    formRenderProps.errors.VALIDATION_SUMMARY && (
                                        <div className={"k-messagebox k-messagebox-error"}>
                                            {formRenderProps.errors.VALIDATION_SUMMARY}
                                        </div>
                                    )}
                                    <div className="mb-4">
                                        <Field
                                            name={"username"}
                                            component={ValidatedInput}
                                            label={"username"}
                                            validator={usernameValidator}
                                        />
                                    </div>
                                    <div className="mb-4">
                                        <Field
                                            name={"firstName"}
                                            component={ValidatedInput}
                                            label={"First name"}
                                            validator={nameValidator}
                                        />
                                    </div>
                                    <div className="mb-4">
                                        <Field
                                            name={"lastName"}
                                            component={ValidatedInput}
                                            label={"Last name"}
                                            validator={nameValidator}
                                        />
                                    </div>
                                </fieldset>
                                <div className="k-form-buttons">
                                    <button
                                        type={"submit"}
                                        className="k-button k-button-md k-rounded-md k-button-solid k-button-solid-base"
                                        disabled={!formRenderProps.allowSubmit}
                                    >
                                        Submit
                                    </button>
                                </div>
                            </FormElement>
                        )}
                    />
                </Dialog>
            )}
        </>
    );
};

export default AddUser;