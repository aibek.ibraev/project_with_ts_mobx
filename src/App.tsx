import * as React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import MainPage from "./pages/mainPage";
import UserDetailPage from "./pages/userDetailPage";
import './App.css';

const App = () => (
    <Router>
        <Switch>
            <Route component={MainPage} path='/' exact />
            <Route component={UserDetailPage} path='/user/:id' exact />
        </Switch>
    </Router>
);
export default App;

