export interface UserCategory {
    CategoryID?: number,
    CategoryName?: string,
    Description?: string,
    details?: any
}

export interface UserItem {
    id?: number,
    username?: string,
    firstName?: string,
    lastName?: string,
    fullName?: string,
    lastLogin?: Date,
    completed?: boolean,
    inEdit?: boolean | string,
}


export interface columnInterface {
    title?: string,
    field?: string,
    show?: boolean,
    filter?: "boolean" | "numeric" | "text" | "date" | undefined,
    minWidth?: number,
    minGridWidth?: number,
    locked?: boolean,
    width?: string | number
}





