import {action, makeObservable, observable} from "mobx";
import {UserItem} from "../interfaces";
import axios from "axios";


export class UserStoreImpl {
    users: UserItem[] = [];
    user: UserItem[] = [];

    constructor() {
        makeObservable(this, {
            users: observable,
            addUser: action,
        })
    }

    fetchUsers() {
        axios.get("http://localhost:3001/users")
            .then((response: { data: UserItem[]; }) => {
                response.data.map((item: UserItem) => {
                    let obj = this.users.find(obj => obj.id === item.id)
                    if(!obj){
                        item.lastLogin = new Date();
                        this.users.push(item);
                    }
                });
            })
            .catch((err: any) => {
                console.log("in axios ", err)
            })
    }

    addUser(user: UserItem){
        const item = {
            id: +Math.random().toFixed(),
            completed: false,
            fullName: `${user.firstName} ${user.lastName}`,
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.username,
            lastLogin: new Date(),
        }
        if((this.users.some(user => user.username === item.username))){
            alert('Такой пользователь уже существует')
        }else{
            axios.post("http://localhost:3001/users", item)
                .then((response: { data: UserItem[]; }) => {
                    console.log(response, 'Добавлено')
                })
                .catch((err: any) => {
                    console.log("Error: ", err)
                })
            this.users.push(item)
        }
    }

    updateUser(user: UserItem){
        const item = {
            id: user.id,
            completed: user.completed,
            fullName: `${user.firstName} ${user.lastName}`,
            firstName: user.firstName,
            lastName: user.lastName,
            username: user.username,
            lastLogin: new Date(),
        }

        axios.put(`http://localhost:3001/users/${item.id}`, item)
            .then((response: { data: UserItem[]; }) => {
                this.users.map(item => {
                    if(item.id === user.id){
                        item = user;
                    }
                    return item
                })
                console.log(response, 'Изменено')
            })
            .catch((err: any) => {
                console.log("Error: ", err)
            })
    }

    getDetailUser(user: UserItem){
        this.user.length = 0
        this.user.push(user)
    }

    searchUsers(search: string) {
        axios.get(`http://localhost:3001/users/?username=${search}`)
            .then((response: { data: UserItem[]; }) => {
                if(response.data.length > 0){
                    this.users.length = 0;
                    response.data.map((item: UserItem) => {
                        this.users.push(item);
                    });
                }
                if(response.data.length === 0){
                    this.fetchUsers()
                }
            })
            .catch((err: any) => {
                console.log("Error ", err)
            })
    }
}

export const  UserStore = new UserStoreImpl();