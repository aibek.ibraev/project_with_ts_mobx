import * as React from "react";
import DetailUser from "../../components/DetailUser";
import {UserStore} from "../../store/user";

const UserDetailPage = () => {
    return(
        <div className='container'>
            <DetailUser userStore={UserStore}/>
        </div>
    );
};
export default UserDetailPage;
