import * as React from "react";
import List from "../../components/List";
import AddUser from "../../components/AddUser";
import {UserStore} from "../../store/user";

const MainPage = () => {
    const [visible, setVisible] = React.useState<boolean>(false);
    const toggleDialog = () => {
        setVisible(!visible);
    };

    return(
        <div className="align-center container">
            <List toggleDialog={toggleDialog} userStore={UserStore}/>
            <AddUser visible={visible} toggleDialog={toggleDialog}  userStore={UserStore}/>
        </div>
    );
};
export default MainPage;
